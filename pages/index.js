import Head from "next/head"
import styles from "../styles/Index.module.css"
export default function Home() {
  return (
    <>

    <main className={styles.main}>
      {/* === Main Section  */}
      <section id='mainSection'  className={styles.section}>

        <div>
          <h4 className={styles.hi} > Hi, my name is </h4>
        </div>

        <div>
          <h3 className={styles.name} > Luis Fonsi. </h3>
        </div>

        <div>
          <h3 className={styles.iDo}>
          I will win your heart.
          </h3>
        </div>

        <div>
          <p className={styles.whatIdo}>
          Despasito was mainly famous because of its music.
           The daddy Yankee part in 
          the song is most famous, 
          even though we dont know the language they are still catchy <a className={styles.upstatement}> Upstatement </a>.
           
          </p>
        </div>

        <div className={styles.songs}>
          Check out my songs
        </div>

        
      </section>
    </main>
    </>
  )
}
