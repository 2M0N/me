import { useEffect } from 'react'
import styles from '../styles/comps/FixedLinks.module.css'


const FixedLinks = () => {

    // Animation []
    useEffect(()=>{
        // Mail animation
        const mainFull = document.getElementById('mainFull');
        mainFull.style.transition='0.8s 1.2s'
        mainFull.style.opacity='1'

        // SVG LINKS
        const svgUlHold = document.getElementById('svgUlHold');
        svgUlHold.style.transition='0.8s 1.2s'
        svgUlHold.style.opacity='1'
    },[])

  return (
   <>
    {/* === EMAIL ID */}
    <div id='mainFull' className={styles.mailFull}>
    <div className={styles.mailHold}>
    <div className={styles.mail}>
        <a>
          luisfonsi@tutanota.com
        </a>
    </div>
    </div>
    </div>



    {/* === Link Icons */}
    <div id='svgUlHold' className={styles.svgUlHold}>
    <ul className={styles.svgUl}>
      {/* === GitLab */}
    <li className={styles.svgli}>
    <svg className={styles.svgIcons}
    viewBox="0 0 24 24" strokeWidth="2" 
    stroke="currentColor" fill="none" 
    > <title> Gitlab </title>
    <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
    <path d="M21 14l-9 7l-9 -7l3 -11l3 7h6l3 -7z" />
    </svg>
    </li>

    {/* === Github */}
    <li className={styles.svgli}>
    <svg  className={styles.svgIcons}
    viewBox="0 0 24 24" strokeWidth="2"  stroke="currentColor"
    fill="none">
      <title> Github </title>
    <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
    <path d="M9 19c-4.3 1.4 -4.3 -2.5 -6 -3m12 5v-3.5c0 -1 .1 -1.4 
    -.5 -2c2.8 -.3 5.5 -1.4 5.5 -6a4.6 4.6 0 0 0 -1.3 -3.2a4.2 4.2 0 
    0 0 -.1 -3.2s-1.1 -.3 -3.5 1.3a12.3 12.3 0 0 0 -6.2 0c-2.4 -1.6 
    -3.5 -1.3 -3.5 -1.3a4.2 4.2 0 0 0 -.1 3.2a4.6 4.6 0 0 0 -1.3 
    3.2c0 4.6 2.7 5.7 5.5 6c-.6 .6 -.6 1.2 -.5 2v3.5" />
    </svg>
    </li>

    {/* === Telegram */}
    <li className={styles.svgli}>
    <svg className={styles.svgIcons}
    viewBox="0 0 24 24" strokeWidth="2"  stroke="currentColor"
    fill="none">
      <title> Telegram </title>
    <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
    <path d="M15 10l-4 4l6 6l4 -16l-18 7l4 2l2 6l3 -4" />
    </svg>
    </li>

    {/* === Twitter   */}
    <li className={styles.svgli}>
    <svg className={styles.svgIcons}
    viewBox="0 0 24 24" strokeWidth="2"  stroke="currentColor" 
    fill="none">
      <title> Twitter </title>
    <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
    <path d="M22 4.01c-1 .49 -1.98 .689 -3 .99c-1.121 -1.265 -2.783 -1.335 -4.38 -.737s-2.643 2.06 -2.62 3.737v1c-3.245 .083 -6.135 -1.395 -8 -4c0 0 -4.182 7.433 4 11c-1.872 1.247 -3.739 2.088 -6 2c3.308 1.803 6.913 2.423 10.034 1.517c3.58 -1.04 6.522 -3.723 7.651 -7.742a13.84 13.84 0 0 0 .497 -3.753c-.002 -.249 1.51 -2.772 1.818 -4.013z" />
    </svg>
    </li>

    {/* === Reddit */}
    <li className={styles.svgli}>
    <svg className={styles.svgIcons}
    viewBox="0 0 24 24" strokeWidth="2"  stroke="currentColor"
    fill="none" >
      <title> Reddit </title>
    <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
    <path d="M12 8c2.648 0 5.028 .826 6.675 2.14a2.5 2.5 0 0 1 2.326 4.36c0 3.59 -4.03 6.5 -9 6.5c-4.875 0 -8.845 -2.8 -9 -6.294l-1 -.206a2.5 2.5 0 0 1 2.326 -4.36c1.646 -1.313 4.026 -2.14 6.674 -2.14z" />
    <path d="M12 8l1 -5l6 1" />
    <circle cx="19" cy="4" r="1" />
    <circle cx="9" cy="13" r=".5" fill="currentColor" />
    <circle cx="15" cy="13" r=".5" fill="currentColor" />
    <path d="M10 17c.667 .333 1.333 .5 2 .5s1.333 -.167 2 -.5" />
    </svg>
    </li>
    </ul>
    </div>
   </>
  )
}

export default FixedLinks