import styles from '../styles/comps/Sidebar.module.css'

const Sidebar = () => {
  return (
   <div className={styles.sidebar} id='sidebarFull'>

    {/* === Sidebar Content */}
    <aside className={styles.aside}>
      <nav className={styles.nav}>
        <ol className={styles.ol}>
          <li className={styles.li}>
            About
          </li>
          <li className={styles.li}>
            Experience
          </li>
          <li className={styles.li}>
            Work
          </li>
          <li className={styles.li}>
            Contact
          </li>

          {/* === Resume  */}
          <a className={styles.resume}> Resume </a>

        </ol>
      </nav>
    </aside>
   </div>
  )
}

export default Sidebar