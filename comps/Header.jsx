import { useState, useEffect } from 'react'
import styles from '../styles/comps/Header.module.css'
import Sidebar from './Sidebar'
import { useMediaQuery } from 'react-responsive'
import FixedLinks from './FixedLinks'


const Header = () => {
  // Sidebar Status 
  const [sidebarOpen,setsidebarOpen] = useState(false)

  // USE MEIDA QUERY
  const isTabletOrMobile = useMediaQuery({query:'(max-width:824px)'})


  // Media Query  AND UNBLUR/ROTATE
  setTimeout(() => {
    function myFunction() {
      if (isTabletOrMobile) { // If media query matches
        return
      } else { // IF MEDIA QUERY DOESN'T MATCH 
        setsidebarOpen(false)
      }
    }
    myFunction()
  },0);

  // USE EFFECT TO UNBLUR AND ROTATE MENU BACK
  // DEF SIDEBAR OPEN/FALSE
  useEffect(()=>{
    // UNBLUR
    if(sidebarOpen === false) { // IF SIDE BAR OPEN 
      document.getElementById('mainSection').style.filter="none";
      document.getElementById('svgMenu').style.transform="rotate(0deg)"
    }
  },[sidebarOpen])

  // Sidebar onClick 
  const sidebarClick = () => {
    const svgMenu = document.getElementById('svgMenu');
    const svgX = document.getElementById('svgX');
    // FULL SIDE BAR
    const sidebarFull = document.getElementById('sidebarFull');

    // Anim if Sidebar False
    if(sidebarOpen == false) {
      svgMenu.style.transform="rotate(90deg)"
      document.getElementById('mainSection').style.filter="blur(8px)";
    }

    // Anim if Sidebar True 
    if(sidebarOpen == true) {
      sidebarFull.style.width='0px'
      sidebarFull.style.transition='0.2s'
      svgX.style.transform="rotate(0deg)" 
      document.getElementById('mainSection').style.filter="none";
    }

    // SIDEBAR ON/OFF
    setTimeout(()=>{
      setsidebarOpen((p)=>!p)
    },200)  // 200 FOR TRANSITION WHEN CLOSING  
  }

  return (
    <>
    <nav id='head' className={styles.head}>

      {/* === SVG Main Icon */}
      <div>
      <svg className={styles.svgCode} viewBox="0 0 20 20" fill="currentColor">
      <path fillRule="evenodd" d="M12.316 3.051a1 1 0 01.633 1.265l-4 12a1 1 
      0 11-1.898-.632l4-12a1 1 0 011.265-.633zM5.707 6.293a1 1 0 010 1.414L3.414
      10l2.293 2.293a1 1 0 11-1.414 1.414l-3-3a1 1 0 010-1.414l3-3a1 1 0
      011.414 0zm8.586 0a1 1 0 011.414 0l3 3a1 1 0 010 1.414l-3 3a1 1 0 
      11-1.414-1.414L16.586 10l-2.293-2.293a1 1 0 010-1.414z" clipRule="evenodd" />
      </svg>
      </div>

      {/* === To Show Sidebar */}
      {sidebarOpen &&  <Sidebar sidebarClick={sidebarClick} />}



      {/* === Not For Desktop 
          === Menu and X Icon */}
      <div className={styles.menuAndX}>
      {sidebarOpen ? (
      <svg onClick={()=>sidebarClick()} id="svgX"
      className={styles.svgX} fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth={2}>
      <path strokeLinecap="round" strokeLinejoin="round" d="M6 18L18 6M6 6l12 12" />
      </svg>
      ):(
        <svg onClick={sidebarClick} id='svgMenu'
      className={styles.svgMenu} viewBox="0 0 20 20" fill="currentColor">
      <path fillRule="evenodd" d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 
      0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM9 15a1 1 0 
      011-1h6a1 1 0 110 2h-6a1 1 0 01-1-1z" clipRule="evenodd" />
      </svg>
      )}
      </div>

      {/* === Nav Links For Desktop  */}
      <nav className={styles.navLinks}>
        <ol className={styles.ol}>
          <li className={styles.li}>
            About
          </li>
          <li className={styles.li}>
            Experience
          </li>
          <li className={styles.li}>
            Work
          </li>
          <li className={styles.li}>
            Contact
          </li>

          {/* === Resume  */}
          <a className={styles.resume}> Resume </a>

        </ol>
      </nav>

      <FixedLinks />
    </nav>

    </>

  )
}

export default Header